# PubSub Notifier

This project demonstrate a simple Publisher/ Subscriber model using node.js. 

The publishing server listens on port 8000 by default and accept request for subscription on specific endpoints and topics. When a data is received on a specific topic, the data is broadcast to all active subscribers.

The subscribing server is node.js express server that exposes three endpoints `/`, `/test1` and `/test2` and listens on port 9000 by defaults. Given that the server is active it will receive message/data on specific topics to which it subscribe if new messages on the topic is available and published by the publishing server. Multiple instance of this server can be started by providing different listening ports in your environment like so `export PORT=9001 & node sub-server.js`

## Starting and testing the application

To run the application, clone the repository and run `cd pubsub` to change your working directory. Take the following step afterward

`npm install` - install dependencies

### Testing

Some simple tests is available in the `./test.js` file to ensure the functionality of the application. Use the test command to run the test.

Run `npm run start-sub` in one terminal and in another terminal run `npm test` `

**NOTE:** You can test the by running the `./make-request.sh` script. In this script, I have coded all the manual steps required to test the application which you can run in one command, `./make-request.sh` just like that. This script will create three output files `sub9000.txt`, `sub9001.txt` and `pub8000.txt` which contains output from the verious servers indicated by the port number. Run `cat sub9000.txt`, `cat sub9001.txt` and `cat pub8000.txt` to view the outputs.  

### Starting
To Start the subscribing server and  the publishing server, you can either run the `start-server.sh` script like so `./start-server.sh` or run the following command.

`npm run start-sub &` - start the subscribing server in background

`npm run start-pub &` - start the publishing server in backround

The `start-server.sh` script will start one instance of the publishing server on port 8000 and two instances of the subscribing server on port `9000` and `9001` respectively. Once the servers are started you can begin subscribing and publishing to various topics and endpoints. The publishing server will notify the subscribing server(s) of messages on topic they are subscribe to.

**Example**

`curl -X POST -H "Content-Type: application/json" -d '{ "url": "http://localhost:9000/test1"}' http://localhost:8000/subscribe/topic1` subscribe to `topic1` on the `/test1` endpoint. 

`curl -X POST -H "Content-Type: application/json" -d '{"message": "Message on topic topic1"}' http://localhost:8000/publish/topic1` post a message to `topic1` topic.

## RUN in Docker

The Dockerfile present in the root directory of the project contain configuration to build docker image that will run the applicaton in an isolated environment. To run the servers in Docker follow the steps below.

`docker build -t pubsub-img:latest .` Build the docker images. During the build, npm test will be run against the servers and the manual tests in `make-request.sh` will also run. The output of this test as describe above will be available in`pub8000.txt`, `sub9000.txt` and `sub9001.txt` in the working directory

`docker run --rm --name supsub-container-contai -v "$(pwd)/output":/output --entrypoint "./copy-output.sh" pubsub-img:latest;` copy the output of running the `make-request.s`h script within docker to the host. A new directory `./output` containing the files will be created in current working directory.

`docker run --name supsub-container -v "$(pwd)/output":/output -p 8000:8000 -p 9000:9000 -p 9001:9001 pubsub-img:latest;` Run docker container.  This will start 1 instance of the publishing server on port 8000 and 2 instances of the subscripting server on port 9000 and 9001. You can interact with the server is the specific ports on the host machine. For example `http://localhost:8000`

`docker exec -it supsub-container sh` Login to the container and interact with the application as needed with the cli

[//] <> (`docker run --name supsub-container -v output:/pubsub pubsub-img:latest /bin/sleep 3600;` run the app in )

[//] <> (docker run --name supsub-container -v /output:/output pubsub-img:latest sleep 3600;)

**Note** please note that manage processes in an automated way, some commands require the `lsof` to be install. To avoid dealing with error messages consider installing the utils.

`yum install lsof` on CentOS

`apk add lsof` on alpine

`apt-get install -y lsof` on ubuntu 
