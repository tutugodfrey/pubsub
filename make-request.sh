#! /bin/bash

# Start a subscriber 1
export PORT=9000; node sub-server.js > sub9000.txt &

# Start subscriber 2
export PORT=9001; node sub-server.js > sub9001.txt &

# Start the publisher
export PORT=8000; node pub-server.js > pub8000.txt &

sleep 4
curl -X POST -H "Content-Type: application/json" -d '{ "url": "http://localhost:9000/test1"}' http://localhost:8000/subscribe/topic1;
curl -X POST -H "Content-Type: application/json" -d '{ "url": "http://localhost:9001/test1"}' http://localhost:8000/subscribe/topic1;
curl -X POST -H "Content-Type: application/json" -d '{ "url": "http://localhost:9000/test1"}' http://localhost:8000/subscribe/read;
curl -X POST -H "Content-Type: application/json" -d '{ "url": "http://localhost:9001/test1"}' http://localhost:8000/subscribe/read;
curl -X POST -H "Content-Type: application/json" -d '{ "url": "http://localhost:9000/test1"}' http://localhost:8000/subscribe/chats;
curl -X POST -H "Content-Type: application/json" -d '{ "url": "http://localhost:9001/test1"}' http://localhost:8000/subscribe/emails;
curl -X POST -H "Content-Type: application/json" -d '{ "url": "http://localhost:9000/test2"}' http://localhost:8000/subscribe/topic2;
curl -X POST -H "Content-Type: application/json" -d '{ "url": "http://localhost:9001/test2"}' http://localhost:8000/subscribe/topic2;

curl -X POST -H "Content-Type: application/json" -d '{"message": "Message on topic topic1"}' http://localhost:8000/publish/topic1;
curl -X POST -H "Content-Type: application/json" -d '{"message": "Message on topic read"}' http://localhost:8000/publish/read;
curl -X POST -H "Content-Type: application/json" -d '{"message": "Message on topic chats"}' http://localhost:8000/publish/chats;
curl -X POST -H "Content-Type: application/json" -d '{"message": "message on topic emails"}' http://localhost:8000/publish/emails;
curl -X POST -H "Content-Type: application/json" -d '{"message": "message on topic topic2"}' http://localhost:8000/publish/topic2;

sleep 4

# close the connections 
# You may need to install lsof or have to manually handle ending process
kill $(lsof -ti  :8000); kill $(lsof -ti :9000); kill $(lsof -ti :9001);
