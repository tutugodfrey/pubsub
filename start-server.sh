#! /bin/bash

# Start a subscriber 1
export PORT=9000; node sub-server.js > sub9000.txt &

# Start subscriber 2
export PORT=9001; node sub-server.js > sub9001.txt &

# Start the publisher
# export PORT=8000; node pub-server.js > pub8000.txt &
export PORT=8000; node pub-server.js
