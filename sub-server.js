const bodyParser = require('body-parser');
const express = require('express');

const port = process.env.PORT || 9000;
const app = express();
app.use(bodyParser.json());
app.get('/', (req, res) => {
  return res.send({
    message: `Hello! This is a subscribing server on PORT ${port}`,
  });
});

app.post('/test1', (req, res) => {
  const { topic, data } = req.body;

  if (topic && data) {
    console.log(topic, data);
    return res.status(200).send({
      message: `Data for topic ${topic} received`,
    });
  };
  return res.status(500)
    .json({ message: 'Error! No data payload received!' });
});

app.post('/test2', (req, res) => {
  const { topic, data } = req.body;

  if (topic && data) {
    console.log(topic, data);
    return res.status(200).send({
      message: `Data for topic ${topic} received`,
    });
  };
  return res.status(500)
    .json({ message: 'Error! No data payload received!' });
});

app.listen(port, () => {
  console.log(`Subscribing server is listening on PORT ${port}`);
});

module.exports = app;
