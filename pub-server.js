const bodyParser = require('body-parser');
const express = require('express');
const request = require('request');

const port = process.env.PORT || 8000;
const app = express();
const subscriptions = {};
const makeRequest = (server, topic, data) => {
  const result = new Promise((resolve, reject) => {
    return request.post(server, {
      json: {
        data,
        topic,
      }
    }, (err, res, body) => {
      if (err) {
        return reject(err);
      };
      return resolve(body);
    });
  });
  return result;
}

app.use(bodyParser.json());
app.get('/', (req, res) => {
  return res.send({
    message: 'Welcome! This is the publishing server',
  });
});

app.post('/subscribe/:topic', (req, res) => {
  const { topic } = req.params;
  const { url } = req.body;
  if (subscriptions[topic]) {
    subscriptions[topic].push(url);
  } else {
    subscriptions[topic] = [url];
  }
  return res.status(201).json({
    topic,
    url,
  });
});

app.post('/publish/:topic', (req, res) => {
  const data = req.body;
  const { topic } = req.params;
  const subs = subscriptions[topic];
  if (!Object.keys(data).length) {
    return res.status(400).json({
      message: `Bad request! Please send data for topic ${topic}`,
    });
  }

  if (!subs) {
    return res.status(404).json({
      message: `No active subscription for topic ${topic} `,
    });
  }

  if (subs.length) {
    Promise.all(subs.map((server) => makeRequest(server, topic, data)))
    .then(result => {
      return res.status(201).json({
        message: 'Success! Subscription published to all active subscribers',
      });
    })
    .catch(( /* err obj */ ) => res.status(500).json({
      message: 'Error! Subscription could not be published',
    }));
  }
});

app.listen(port, () => {
  console.log(`Publishing server is listening on PORT ${port}`);
});

module.exports = app;
