const chai = require('chai');
const chaiHttp = require('chai-http');

const pubServer = require('./pub-server');
const { expect } = chai;
chai.use(chaiHttp);

describe('Testing pubsub', () => {
  describe('Subscribe server', () => {
    it('Confirm subscribe server is reachable', () => {
      chai.request('http://localhost:9000')
        .get('/')
        .then(res => {
          expect(res.status).to.equal(200);
          expect(res.body)
            .to.have.property('message')
            .to.equal('Hello! This is a subscribing server on PORT 9000');
        })
        .catch(err => console.log(err));
    })
  });

  describe('Subscribing', () => {
    it('base /', () => {
      return chai.request(pubServer).get('/').then(res => {
        expect(res.status).to.equal(200);
        expect(res.body)
          .to.have.property('message')
          .to.equal('Welcome! This is the publishing server');
      });
    });

    it('subscribe to topic reading', () => {
      const topic = 'reading';
      const url = 'http://localhost:9000/test1';
      return chai.request(pubServer)
        .post(`/subscribe/${topic}`)
        .set('Content-Type', 'application/json')
        .send({
          url,
        })
        .then(res => {
          expect(res.body).to.have.property('topic').to.equal(topic);
          expect(res.body).to.have.property('url').to.equal(url);
        })
        .catch(err => console.log(err));
    });

    it('subscribe to topic emails', () => {
      const topic = 'emails';
      const url = 'http://localhost:9000/test1';
      return chai.request(pubServer)
        .post(`/subscribe/${topic}`)
        .set('Content-Type', 'application/json')
        .send({
          url,
        })
        .then(res => {
          expect(res.body).to.have.property('topic').to.equal(topic);
          expect(res.body).to.have.property('url').to.equal(url);
        })
        .catch(err => console.log(err));
    });

    it('subscribe to topic chats', () => {
      const topic = 'chats';
      const url = 'http://localhost:9000/test1';
      return chai.request(pubServer)
        .post(`/subscribe/${topic}`)
        .set('Content-Type', 'application/json')
        .send({
          url,
        })
        .then(res => {
          expect(res.status).to.equal(201);
          expect(res.body).to.have.property('topic').to.equal(topic);
          expect(res.body).to.have.property('url').to.equal(url);
        })
        .catch(err => console.log(err));
    });
  });

  describe('Publishing to topics', () => {
    it('should not publish for topic without data', () => {
      const topic = 'reading';
      const url = 'http://localhost:9000/test1'
      return chai.request(pubServer)
        .post(`/publish/${topic}`)
        .set('Content-Type', 'application/json')
        .send()
        .then(res => {
          expect(res.status).to.equal(400);
          expect(res.body)
            .to.have.property('message')
            .to.equal(`Bad request! Please send data for topic ${topic}`);
        })
        .catch(err => console.log(err));
    });

    it('should alert when there are no active subscription for a topic', () => {
      const topic = 'chat';
      const url = 'http://localhost:9000/test1'
      return chai.request(pubServer)
        .post(`/publish/${topic}`)
        .set('Content-Type', 'application/json')
        .send({
          url,
        })
        .then(res => {
          expect(res.status).to.equal(404);
          expect(res.body)
            .to.have.property('message')
            .to.equal(`No active subscription for topic ${topic} `);
        })
        .catch(err => console.log(err));
    });

    it('publish to topic reading', () => {
      const topic = 'reading';
      const url = 'http://localhost:9000/test1'
      return chai.request(pubServer)
        .post(`/publish/${topic}`)
        .set('Content-Type', 'application/json')
        .send({
          url,
        })
        .then(res => {
          expect(res.status).to.equal(201);
          expect(res.body)
            .to.have.property('message')
            .to.equal('Success! Subscription published to all active subscribers');
        })
        .catch(err => console.log(err));
    });

    it('publish to topic chats', () => {
      const topic = 'chats';
      const url = 'http://localhost:9000/test1'
      return chai.request(pubServer)
        .post(`/publish/${topic}`)
        .set('Content-Type', 'application/json')
        .send({
          url,
        })
        .then(res => {
          expect(res.status).to.equal(201);
          expect(res.body)
            .to.have.property('message')
            .to.equal('Success! Subscription published to all active subscribers');
        })
        .catch(err => console.log(err));
    });

    it('publish to topic emails', () => {
      const topic = 'emails';
      const url = 'http://localhost:9000/test1'
      return chai.request(pubServer)
        .post(`/publish/${topic}`)
        .set('Content-Type', 'application/json')
        .send({
          url,
        })
        .then(res => {
          expect(res.status).to.equal(201);
          expect(res.body)
            .to.have.property('message')
            .to.equal('Success! Subscription published to all active subscribers');
        })
        .catch(err => console.log(err));
    });
  });
});