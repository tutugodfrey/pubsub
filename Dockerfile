FROM node:alpine
LABEL maintainer="Godfrey Tutu <godfrey_tutu@yahooo.com>"
LABEL description="Build image to run pubsub app, expose ports to connect to host."
RUN apk update; apk upgrade; apk add bash; apk add curl; apk add lsof;
WORKDIR /pubsub
COPY package* ./
RUN npm install
COPY pub-server.js sub-server.js test.js start-server.sh make-request.sh copy-output.sh ./
RUN npm test
RUN ./make-request.sh
EXPOSE 8000 9000 9001
ENTRYPOINT ./start-server.sh
